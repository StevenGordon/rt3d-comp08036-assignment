#ifndef SKYBOX_H
#define SKYBOX_H

#include "rt3d.h"
#include <gl\glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>

class Skybox
{

public:

	Skybox();

	void init();

	void draw(std::stack<glm::mat4> mvStack);

	GLuint getVertCount() { return vertCount; }

	GLfloat * getVerts() { return verts; }

	GLuint getIndexCount() { return indexCount; }

	GLuint * getIndices() { return indices; }

	GLfloat * getTexCoords() { return texCoords; }

	GLuint getTexture(int i) { return textures[i]; }

	GLuint getShader() { return shader; }

	~Skybox() { }

private:
	GLuint mesh;

	GLuint shader;

	GLuint vertCount;
	GLfloat verts[12];

	GLuint indexCount;
	GLuint indices[6];

	GLfloat texCoords[8];

	GLuint textures[5];
};

#endif