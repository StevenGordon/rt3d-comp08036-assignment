#include "Npc.h"

void Npc::init(const char *modelFilename, char *textureFileName)
{
	texture = rt3d::loadBitmap(textureFileName);
	mesh = Model.ReadMD2Model(modelFilename);
	md2VertCount = Model.getVertDataSize();
}

void Npc::Animate(int CurrentAnimation, GLfloat speed)
{
	Model.Animate(CurrentAnimation, speed);
}

