#include "Enemy.h"

Enemy::Enemy(glm::vec3 iPosition, GLfloat iRotate) : Npc ( iPosition, iRotate )
{
	currentAnim = 0;
}

void Enemy::draw(std::stack<glm::mat4> mvStack, GLuint shader, GLfloat scale)
{
	mvStack.top() = glm::translate(mvStack.top(), pos );
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));	// Correct orientation
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 0.0f, 1.0f));		// Turn to face +ve X
	mvStack.top() = glm::rotate(mvStack.top(), rotate , glm::vec3(0.0f, 0.0f, 1.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.05, scale*0.05, scale*0.05));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(mesh, md2VertCount/3, GL_TRIANGLES);
}

void Enemy::enemyUpdate(GLfloat moveDistance, Player * player)
{
	GLfloat alertDistance = 10;
	GLfloat playerDistance;

	// Get difference in vertical and horizontal components (x and z)
	playerDistanceX = pos.x - (player->getPos().x);
	playerDistanceZ = pos.z - (player->getPos().z);

	playerDistance = playerDistanceX + playerDistanceZ;
	// If playerDistance^2 is less than or equal to alertDistance^2 AND playerDistance^2 is greater than or equal to -(alertDistance^2)
	canSeePlayer = playerDistance*playerDistance <= alertDistance*alertDistance && playerDistance*playerDistance >= -(alertDistance*alertDistance);
	// If player distance is less than (alerDistance/2)^2
	hostile = (playerDistance*playerDistance) < ((alertDistance/2)*(alertDistance/2));
	alert(moveDistance, player);
}

void Enemy::alert(GLfloat moveDistance, Player * player)
{
	if (canSeePlayer)
	{
		if (!player->getCrouched())
		{	
			// Find angle the player is in relation to enemy
			GLfloat tanR = atan2(player->getPos().x - pos.x, player->getPos().z - pos.z) * RADIAN_TO_DEG;
			// Reverse the angle to get enemy to face player
			rotate = tanR + 180;
			// Alert animation
			currentAnim = 9;
		}
		if(hostile)
		{
			// Move towards player
			pos = rt3d::moveForward(pos, -rotate, moveDistance);
			currentAnim = 1;
		}
	}
	else currentAnim = 0;
}
