#include "CollBox.h"

CollBox::CollBox(glm::vec3 ipos)
{
	pos.x = ipos.x; pos.y = ipos.y; pos.z = ipos.z;
}

void CollBox::init()
{
	rt3d::loadObj("cube.obj", verts, norms, tex_coords, indices);
	size = indices.size();
	meshIndexCount = size;
	mesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
}

void CollBox::setCollisonBox(glm::vec3 centerofObject, glm::vec3 scale)
{
	// Sets collBox pos to new poistion
	pos.x = centerofObject.x; pos.y = centerofObject.y; pos.z = centerofObject.z;
	// All collBoxes are (-1,-1,-1) to (1,1,1) therefore teh scale becomes half of the width
	dimensions.x = scale.x; dimensions.y = scale.y; dimensions.z = scale.z;
}

	/* corrects overlap by moving this object by overlap */
glm::vec3 CollBox::overlap(CollBox * box, int i)
{
	glm::vec3 overlap;

	overlap.y = 0.0f;
		/* For when this collBox is smaller than the colliding collBox */
	if (i > 2)
	{
		if (box->getPos().x + box->getDimensions().x > pos.x + dimensions.x
			&& box->getPos().x - box->getDimensions().x < pos.x - dimensions.x)
		{
			if(box->getPos().z  - box->getDimensions().z < pos.z - dimensions.z)

				overlap.z = (box->getPos().z + box->getDimensions().z) - (pos.z - dimensions.z);

			else if(box->getPos().z + box->getDimensions().z > pos.z + dimensions.z)

				overlap.z = (box->getPos().z - box->getDimensions().z) - (pos.z + dimensions.z);
		}
		if (box->getPos().z + box->getDimensions().z > pos.z + dimensions.z
			&& box->getPos().z - box->getDimensions().z < pos.z - dimensions.z)
		{
			if(box->getPos().x - box->getDimensions().x < pos.x - dimensions.x)

				overlap.x = (box->getPos().x + box->getDimensions().x) - (pos.x - dimensions.x);

			else if(box->getPos().x + box->getDimensions().x > pos.x + dimensions.x)

				overlap.x = (box->getPos().x - box->getDimensions().x) - (pos.x + dimensions.x);
		}
	}
		/* For when this collBox is the same size as the colliding collBox */
	else if (i < 2)
	{
		if (box->getPos().x + box->getDimensions().x > pos.x + dimensions.x)
			overlap.x = (box->getPos().x - box->getDimensions().x) - (pos.x + dimensions.x);

		else if (box->getPos().x - box->getDimensions().x < pos.x - dimensions.x)
			overlap.x = (box->getPos().x + box->getDimensions().x) - (pos.x - dimensions.x);

		if (box->getPos().z + box->getDimensions().z > pos.z + dimensions.z)
			overlap.z = (box->getPos().z - box->getDimensions().z) - (pos.z + dimensions.z);

		else if (box->getPos().z  + box->getDimensions().z > pos.z - dimensions.z)
			overlap.z = (box->getPos().z + box->getDimensions().z) - (pos.z - dimensions.z);
	}
	
	return overlap;
}

bool CollBox::Colide(CollBox * colBox)
{
	bool collided = true;
	
	if( pos.x + dimensions.x < colBox->pos.x - colBox->dimensions.x )
		collided = false;
	if( pos.x - dimensions.x > colBox->pos.x + colBox->dimensions.x )
		collided = false;
	if( pos.z - dimensions.z > colBox->pos.z + colBox->dimensions.z)
		collided = false;
	if( pos.z + dimensions.z < colBox->pos.z - colBox->dimensions.z)
		collided = false;
		
	return collided;
}
