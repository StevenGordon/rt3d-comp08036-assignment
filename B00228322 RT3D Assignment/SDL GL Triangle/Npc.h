#pragma once
#ifndef NPC_H
#define NPC_H

#include <glm/glm.hpp>
#include <SDL.h>
#include <stack>
#include "md2model.h"

class Npc
{

public:
	Npc(glm::vec3 iPosition, GLfloat iRotate)
	{ pos.x = iPosition.x; pos.y = iPosition.y; pos.z = iPosition.z; rotate = iRotate; }

	virtual void draw(std::stack<glm::mat4> mvStack, GLuint shader, GLfloat scale) = 0;

	void init(const char *modelFilename, char *textureFileName);

	glm::vec3 getPos() { return pos; }
	void setPos( glm::vec3 set )
	{
		pos.x = set.x;
		pos.y = set.y;
		pos.z = set.z;
	}

	void overlapCorrect( glm::vec3 correction )
	{
		pos.x = pos.x + correction.x;
		//pos.y = pos.y + correction.y;
		pos.z = pos.z + correction.z;
	}

	GLfloat getRotate() { return rotate; }
	void setRotate( GLfloat newRotate ) { rotate = newRotate; }
	void addRotate(GLfloat angle) { rotate += angle; }

	GLuint getTexture() { return texture; }

	md2model &getModel() { return Model; }

	GLuint getMesh() { return mesh; }

	GLuint getVertCount() { return md2VertCount; }

	void Animate(int CurrentAnimation, GLfloat speed);

	int getCurrentAnim() { return currentAnim; }
	void setAnim(int newAnim ) { currentAnim = newAnim; }

	virtual ~Npc() { return; }

protected:

	glm::vec3 pos;
	glm::vec2 colBox;

	GLfloat rotate;

	GLuint texture;

	md2model Model;
	GLuint mesh;
	GLuint md2VertCount;

	int currentAnim;

};

#endif