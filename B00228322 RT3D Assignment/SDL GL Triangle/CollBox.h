#pragma once
#ifndef COLLBOX_H
#define COLLBOX_H

#include <glm\glm.hpp>
//#include <stack>
#include "rt3d.h"
#include "rt3dObjLoader.h"

class CollBox
{

public:
	CollBox(glm::vec3 ipos);
	
	void init();
	
	//void draw(std::stack<glm::mat4> mvStack, GLuint shader);
	void setCollisonBox(glm::vec3 centerofObject, glm::vec3 scale);

	bool Colide(CollBox * colBox);

	glm::vec3 overlap(CollBox * box, int i);

	glm::vec3 getPos() { return pos; }
	glm::vec3 getDimensions() { return dimensions; }
	
	~CollBox() { }
	
private:
	glm::vec3 pos;
	glm::vec3 dimensions;

	GLuint size;
	GLuint mesh;
	GLuint meshIndexCount;

	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	
};

#endif
