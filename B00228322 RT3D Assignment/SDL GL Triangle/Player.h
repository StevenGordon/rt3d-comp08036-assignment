#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include <SDL.h>
#include <glm/glm.hpp>
#include "Npc.h"
#include "Camera.h"

class Player : public Npc
{

public:

	Player(glm::vec3 iPosition, GLfloat iRotate);

	void draw(std::stack<glm::mat4> mvStack, GLuint shader, GLfloat scale);
	
	void checkBounds();

	void playerUpdate(Uint8 *keys, GLfloat moveDistance, Camera * cam);

	bool getCrouched() { return crouched; }
	void setCrouched(bool set) { crouched = set; } 

	~Player() {};

private:
	bool crouched;

	bool outBounds;

};

#endif