#include "Objects.h"

void Objects::init(const char *modelFilename, char *textureFileName)
{
	rt3d::loadObj(modelFilename, verts, norms, tex_coords, indices);
	size = indices.size();
	meshIndexCount = size;
	texture = rt3d::loadBitmap(textureFileName);
	mesh = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
}

void Objects::draw(std::stack<glm::mat4> mvStack, GLuint shader)
{
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(mesh,meshIndexCount,GL_TRIANGLES);
}