#pragma once
#ifndef CAMERA_H
#define CAMERA_H

#include <glm\glm.hpp>
#include <GL/glew.h>
#include "rt3d.h"


class Camera {

public:

	Camera(glm::vec3 position, glm::vec3 target, glm::vec3 orientation);

	void CameraUpdate(Uint8 *keys, GLfloat r);

	void idleRotate();
	void resetIdle();

	glm::vec3 getEye() { return eye; }
	void setEyeY(float pos) { eye.y = pos; }
	void setEye(glm::vec3 pos)
	{
		eye.x = pos.x;
		eye.y = pos.y;
		eye.z = pos.z;
	}

	glm::vec3 getAt() { return at; }
	void setAt(glm::vec3 look)
	{
		at.x = look.x;
		at.y = look.y;
		at.z = look.z;
	}

	glm::vec3 getUp() { return up; }

	glm::vec3 getTempEye() { return tempEye; }
	void setTempY(GLfloat newY) {tempEye.y = newY; }

	float getRotate() { return idleR; }

	int getTimer() { return timer; }
	void addTimer(int addTime) { timer += addTime; }

	int getCurrentTime() { return currentTime; }
	void setCurrentTime(int newTime) { currentTime = newTime; }

	int getLastTime() { return lastTime; }
	void setLastTime(int newTime) { lastTime = newTime; }

	int getIdleTimer() { return idleTimer; }

	int deltaTime(int current, int last) { return current - last; }

	bool getIdle() { return idle; }
	void setIdle(bool set) { idle = set; }

	~Camera();

private:
	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;
	glm::vec3 tempEye;

	GLfloat idleR;

	int timer;
	int currentTime;
	int lastTime;
	int idleTimer;

	bool idle;
};

#endif