// Based loosly on the first triangle OpenGL tutorial
// http://www.opengl.org/wiki/Tutorial:_OpenGL_3.1_The_First_Triangle_%28C%2B%2B/Win%29
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include "Skybox.h"
#include "Objects.h"
#include "Camera.h"
#include "Player.h"
#include "Enemy.h"
#include "CollBox.h"

using namespace std;

// Globals
// Real programs don't use globals! ( sorry :( )
GLuint shaderProgram;

stack<glm::mat4> mvStack; 

Skybox * skybox;
Camera * cam;
Player * player;
Enemy * enemy1;
Enemy * enemy2;

CollBox * colBox[6];

Objects * ground;
Objects * wall;
Objects * building1;
Objects * building2;
Objects * building3;

rt3d::lightStruct light0 = {
	{0.3f, 0.3f, 0.3f, 1.0f}, // ambient
	{1.0f, 1.0f, 1.0f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{-10.0f, 10.0f, 10.0f, 1.0f}  // position
};

glm::vec4 lightPos(-10.0f, 10.0f, 10.0f, 1.0f); //light position

rt3d::materialStruct material0 = {
	{0.2f, 0.2f, 0.2f, 1.0f}, // ambient
	{0.7f, 0.7f, 0.7f, 1.0f}, // diffuse
	{0.1f, 0.1f, 0.1f, 1.0f}, // specular
	1.0f  // shininess
};

rt3d::materialStruct material1 = {
	{0.4f, 0.4f, 1.0f, 1.0f}, // ambient
	{0.8f, 0.8f, 1.0f, 1.0f}, // diffuse
	{0.8f, 0.8f, 0.8f, 1.0f}, // specular
	1.0f  // shininess
};

rt3d::materialStruct material2 = {
	{0.4f, 0.4f, 0.8f, 1.0f}, // ambient
	{0.6f, 0.6f, 0.2f, 1.0f}, // diffuse
	{0.1f, 0.1f, 0.1f, 1.0f}, // specular
	1.0f  // shininess
};

// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

void init(void) {
	skybox = new Skybox();

	ground = new Objects(glm::vec3(-25.0f, -0.1f, 25.0f), 0.0f);
	wall = new Objects(glm::vec3(0.0f, -0.1f, 0.0f), 0.0f);

	building1 = new Objects(glm::vec3(0.0f, -0.1f, 0.0f), 0.0f);
	building2 = new Objects(glm::vec3(0.0f, -0.1f, 0.0f), 0.0f);
	building3 = new Objects(glm::vec3(0.0f, -0.1f, 0.0f), 0.0f);
	colBox[3] = new CollBox(glm::vec3(0.0f, -0.1f, 0.0f));
	colBox[4] = new CollBox(glm::vec3(0.0f, -0.1f, 0.0f));
	colBox[5] = new CollBox(glm::vec3(0.0f, -0.1f, 0.0f));

	player = new Player(glm::vec3(0.0f, 1.1f, 25.0f), 90.0f);
	colBox[0] = new CollBox(glm::vec3(0.0f, 1.1f, 25.0f));

	enemy1 = new Enemy(glm::vec3(10.0f, 1.2f, -1.0f), 90.0f);
	enemy2 = new Enemy(glm::vec3(0.0f, 1.2f, -10.0f), 90.0f);
	colBox[1] = new CollBox(glm::vec3(10.0f, 1.2f, -1.0f));
	colBox[2] = new CollBox(glm::vec3(0.0f, 1.2f, -10.0f));
	
	cam = new Camera(glm::vec3 (0.0f, 3.5f, 0.0f), glm::vec3(0.0f, 1.0f, -1.0f), glm::vec3 (0.0f, 1.0f, 0.0f));
	cam->resetIdle();
	cam->setCurrentTime( SDL_GetTicks() );
	cam->setLastTime( SDL_GetTicks() );

	shaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");

	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	
	colBox[0]->init();
	colBox[1]->init();
	colBox[2]->init();
	colBox[3]->init();
	colBox[4]->init();
	colBox[5]->init();
	
	ground->init("cube.obj", "Ground.bmp");
	wall->init("cube.obj", "wall.bmp");
	building1->init("Building1.obj", "Building1.bmp");
	building2->init("Building1.obj", "Building1.bmp");
	building3->init("Building1.obj", "Building1.bmp");

	player->init("yoshi.MD2", "oyoshi.bmp");

	enemy1->init("tris.MD2", "hobgoblin2.bmp");
	enemy2->init("tris.MD2", "hobgoblin2.bmp");

	skybox->init();
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

}

void update(void) {
	Uint8 *keys = SDL_GetKeyboardState(NULL);

	/* collision handling */
	bool collision = false;	// collision flag
	int j, i;
		/* checks all NPCs colBoxes for collision and sets flag to true */
	for(j = 0; j < 3; j++)
	{
		for(i = 0; i < 6; i++)
		{
			if( i != j )
			{
				if(colBox[j]->Colide(colBox[i]))
					collision = true;
			}
			if(collision)
				break;
		}
		if(collision)
			break;
	}

	if (collision)
	{
		// Moves player/ enemies back by the overlap amount
		if( j == 0 )
			player->overlapCorrect( colBox[j]->overlap(colBox[i], i) );
		if( j == 1 )
			enemy1->overlapCorrect( colBox[j]->overlap(colBox[i], i) );
		if( j == 2 )
			enemy2->overlapCorrect( colBox[j]->overlap(colBox[i], i) );
	}

	// Camera Idle timer
	cam->setCurrentTime( SDL_GetTicks() );
	
	if ( SDL_KEYUP )
	{
		if ( cam->getTimer() < cam->getIdleTimer() )
		{
			cam->addTimer( cam->deltaTime(cam->getCurrentTime(), cam->getLastTime()) );
			player->setAnim( 0 );
		}
		else cam->setIdle( true );
	}

	cam->CameraUpdate(keys, player->getRotate());
	player->playerUpdate(keys, 0.25f, cam);
	
	enemy1->enemyUpdate(0.2f, player);
	enemy2->enemyUpdate(0.2f, player);
	
	if ( keys[SDL_SCANCODE_1] ) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_CULL_FACE);
	}
	if ( keys[SDL_SCANCODE_2] ) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_CULL_FACE);
	}

	// IF idle set camera rotation to the cameras stored rotation value
	// this gets incremented when idle when idle to get the camera to rotate
	// around the player ELSE camera rotation is set as players rotation
	if ( cam->getIdle() )
	{
		player->setAnim( 8 );
		cam->setEye( rt3d::moveForward(player->getPos(),cam->getRotate(),-3.0f) );
	}
	else 
	{
		cam->setEye( rt3d::moveForward(player->getPos(),player->getRotate(),-3.0f) );
	}

	cam->setAt( rt3d::moveForward(player->getPos(),player->getRotate(), 1.0f) );

	cam->setEyeY( cam->getTempEye().y );

	cam->setLastTime(cam->getCurrentTime());

	player->setCrouched(false);

		/* Debug for fine tunning of collision detection because its a bit of a mess right now */
	//cout << "DX: " << player->getPos().x << " DZ: " << player->getPos().z << endl;
}

void draw(SDL_Window * window) {
	int i = 0;
	GLfloat terrainX = -25;
	GLfloat terrainSize = 10;

	// clear the screen
	glEnable(GL_CULL_FACE);
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,0.1f,200.0f);

	GLfloat scale(1.0f); // just to allow easy scaling of complete scene
	
	glm::mat4 modelview(1.0); // set base position for scene
	mvStack.push(modelview);	// Push modelview

	mvStack.top() = glm::lookAt(cam->getEye(),cam->getAt(),cam->getUp());	// set up camera
	
		/* Skybox */
	glUseProgram(skybox->getShader());	// Set skybox shader
	rt3d::setUniformMatrix4fv(skybox->getShader(), "projection", glm::value_ptr(projection)); // reset projection
	// Set up skybox
	glDepthMask(GL_FALSE);	// Disable depth mask
	glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top());
	mvStack.push( glm::mat4(mvRotOnlyMat3) );	// Stop skybox rotating with camera

	mvStack.push( mvStack.top() ); // push skybox
	skybox->draw(mvStack);	// Draw skybox
	mvStack.pop(); // pop skybox

	mvStack.pop(); // returning to base modelview matrix

	glUseProgram(shaderProgram);	// Set phong shader
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection)); // reset projection

	rt3d::setLight(shaderProgram,light0);	// Set Light for phong
	
	glm::vec4 tmp = mvStack.top()*lightPos;
	light0.position[0] = tmp.x;
	light0.position[1] = tmp.y;
	light0.position[2] = tmp.z;
	rt3d::setLightPos(shaderProgram, glm::value_ptr(tmp));	// Set new light position
	
	glDepthMask(GL_TRUE); // Enable depth mask

	rt3d::setMaterial(shaderProgram, material0);

		/* Scene */
	// Ground plane creates by 6 lines of terrain
	for (int j = 0; j < 6; j++)
	{
		// This cube is created at the edge of the level
		glBindTexture(GL_TEXTURE_2D, ground->getTexture());
		mvStack.push(mvStack.top());	// Push j line
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(terrainX, 0.0f, 25.0f));
		mvStack.top() = glm::scale(mvStack.top(),glm::vec3(10.0f, 0.1f, 10.0f));
		ground->draw(mvStack, shaderProgram);

		// This cube moves its draw coords in relation to the last
		// creating a line of drawn cubes that span the length of the level
		for(i=0; i < 6; i++)
		{
			mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, -1.0f));
			ground->draw(mvStack, shaderProgram);
		}
		mvStack.pop(); // Pop j line

		terrainX += terrainSize;
	}

		/* Back Wall */
	glBindTexture(GL_TEXTURE_2D, wall->getTexture());
	mvStack.push(mvStack.top());	// Push wall
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(5.0f, -0.1f, 30.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(30.0f, 1.5f, 5.0f));
	wall->draw(mvStack, shaderProgram);
	mvStack.pop(); // Pop wall

	glBindTexture(GL_TEXTURE_2D, wall->getTexture());
	mvStack.push(mvStack.top());	// Push wall
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-25.0f, -0.1f, 30.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(30.0f, 1.5f, 5.0f));
	wall->draw(mvStack, shaderProgram);
	mvStack.pop(); // Pop wall

	/* Front Wall */
	glBindTexture(GL_TEXTURE_2D, wall->getTexture());
	mvStack.push(mvStack.top());	// Push wall
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(5.0f, -0.1f, -35.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(30.0f, 1.5f, 5.0f));
	wall->draw(mvStack, shaderProgram);
	mvStack.pop(); // Pop wall

	glBindTexture(GL_TEXTURE_2D, wall->getTexture());
	mvStack.push(mvStack.top());	// Push wall
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-25.0f, -0.1f, -35.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(30.0f, 1.5f, 5.0f));
	wall->draw(mvStack, shaderProgram);
	mvStack.pop(); // Pop wall

	/* Right Wall */
	glBindTexture(GL_TEXTURE_2D, wall->getTexture());
	mvStack.push(mvStack.top());	// Push wall
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.1f, 30.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(30.0f, 1.5f, 5.0f));
	wall->draw(mvStack, shaderProgram);
	mvStack.pop(); // Pop wall

	glBindTexture(GL_TEXTURE_2D, wall->getTexture());
	mvStack.push(mvStack.top());	// Push wall
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-30.0f, -0.1f, 30.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(30.0f, 1.5f, 5.0f));
	wall->draw(mvStack, shaderProgram);
	mvStack.pop(); // Pop wall

		/* Left Wall */
	glBindTexture(GL_TEXTURE_2D, wall->getTexture());
	mvStack.push(mvStack.top());	// Push wall
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, -1.0f, 0.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.1f, 20.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(30.0f, 1.5f, 5.0f));
	wall->draw(mvStack, shaderProgram);
	mvStack.pop(); // Pop wall

	glBindTexture(GL_TEXTURE_2D, wall->getTexture());
	mvStack.push(mvStack.top());	// Push wall
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, -1.0f, 0.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-30.0f, -0.1f, 20.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(30.0f, 1.5f, 5.0f));
	wall->draw(mvStack, shaderProgram);
	mvStack.pop(); // Pop wall
	
		/* Buildings */
	glBindTexture(GL_TEXTURE_2D, building1->getTexture());
	mvStack.push(mvStack.top());	// Push Buildings
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(15.0f, 0.0f, 15.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, -1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(1.0f, 1.5f, 1.0f));
	building1->draw(mvStack, shaderProgram);
	colBox[3]->setCollisonBox(glm::vec3(15.0f, 0.0f, 15.0f), glm::vec3(6.0f, 1.0f, 7.0f));
	mvStack.pop();	// Pop Buildings
	
	glBindTexture(GL_TEXTURE_2D, building2->getTexture());
	mvStack.push(mvStack.top());	// Push Buildings
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-5.0f, 0.0f, 5.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(1.0f, 1.5f, 1.0f));
	building3->draw(mvStack, shaderProgram);
	colBox[4]->setCollisonBox(glm::vec3(-5.0f, 0.0f, 5.0f), glm::vec3(6.0f, 1.0f, 7.0f));
	mvStack.pop();	// Pop Buildings

	glBindTexture(GL_TEXTURE_2D, building3->getTexture());
	mvStack.push(mvStack.top());	// Push Buildings
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(15.0f, 0.0f, -15.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, -1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(1.0f, 1.5f, 1.0f));
	building2->draw(mvStack, shaderProgram);
	colBox[5]->setCollisonBox(glm::vec3(15.0f, 0.0f, -15.0f), glm::vec3(6.0f, 1.0f, 7.0f));
	mvStack.pop();	// Pop Buildings

		/* Player */
	// Animate the md2 model, and update the mesh with new vertex data
	player->getModel().Animate(player->getCurrentAnim(),0.4f);
	rt3d::updateMesh(player->getMesh(),RT3D_VERTEX,player->getModel().getAnimVerts(),player->getVertCount());

	// draw the player
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, player->getTexture());
	rt3d::materialStruct tmpMaterial = material2;
	rt3d::setMaterial(shaderProgram, tmpMaterial);
	mvStack.push(mvStack.top());	// Push player
	player->draw(mvStack, shaderProgram, scale);
	colBox[0]->setCollisonBox(player->getPos(), glm::vec3(1.0f, 1.0f, 1.0f));
	mvStack.pop();	// Pop player
	glCullFace(GL_BACK);
	
	// Animate the md2 model, and update the mesh with new vertex data
	enemy1->getModel().Animate(enemy1->getCurrentAnim(),0.2f);
	rt3d::updateMesh(enemy1->getMesh(),RT3D_VERTEX,enemy1->getModel().getAnimVerts(),enemy1->getVertCount());

	// draw the Enemy1
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, enemy1->getTexture());
	rt3d::materialStruct tmpMaterial1 = material1;
	rt3d::setMaterial(shaderProgram, tmpMaterial1);
	mvStack.push(mvStack.top());	// Push Enemy1
	enemy1->draw(mvStack, shaderProgram, scale);
	colBox[1]->setCollisonBox(enemy1->getPos(), glm::vec3(1.0f, 1.0f, 1.0f));
	mvStack.pop();	// Pop Enemy1
	glCullFace(GL_BACK);

	// Animate the md2 model, and update the mesh with new vertex data
	enemy2->getModel().Animate(enemy2->getCurrentAnim(),0.2f);
	rt3d::updateMesh(enemy2->getMesh(),RT3D_VERTEX,enemy2->getModel().getAnimVerts(),enemy2->getVertCount());

	// draw the Enemy2
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, enemy2->getTexture());
	mvStack.push(mvStack.top());	// Push Enemy2
	enemy2->draw(mvStack, shaderProgram, scale);
	colBox[2]->setCollisonBox(enemy2->getPos(), glm::vec3(1.0f, 1.0f, 1.0f));
	mvStack.pop();	// Pop Enemy2
	glCullFace(GL_BACK);
	
	mvStack.pop(); // initial matrix
	glDepthMask(GL_TRUE);

    SDL_GL_SwapWindow(window); // swap buffers
}

// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
	
	const int FPS = 30;
	
	double frameTime = 1.0 / (double)FPS;
    frameTime = frameTime * 1000;
    int milliFrameTime = (int)frameTime;
    int sleep;

	Uint32 lastTick;
    Uint32 currentTick;
	
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		
		lastTick = SDL_GetTicks();
		
		update();
		draw(hWindow); // call the draw function

		currentTick = SDL_GetTicks();
		sleep = milliFrameTime - (currentTick - lastTick);
		// If desired frametime - actual frametime is less than 0 set sleep = 0
		// if desired frametime > actual frametime then sleep the difference
		if(sleep < 0) { sleep = 0; }
		SDL_Delay(sleep);

	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}