#include "Player.h"

Player::Player(glm::vec3 iPosition, GLfloat iRotate) : Npc(iPosition, iRotate)
{
	 currentAnim = 0;
	 crouched = false;
	 outBounds = false;
}

void Player::draw(std::stack<glm::mat4> mvStack, GLuint shader, GLfloat scale)
{
	mvStack.top() = glm::translate(mvStack.top(), pos );
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));	// player axis is now corrected (Y in now up)
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 0.0f, 1.0f));		// player now facing in +ve X direction
	mvStack.top() = glm::rotate(mvStack.top(), rotate , glm::vec3(0.0f, 0.0f, -1.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.05, scale*0.05, scale*0.05));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(mesh, md2VertCount/3, GL_TRIANGLES);
}

void Player::checkBounds()
{
	if( pos.x > 29.0f )
		pos.x = 29.0f;

	else if( pos.x < -19.0f )
		pos.x = -19.0;

	if( pos.z < -29.0f )
		pos.z = -29.0f;

	else if( pos.z > 29 )
		pos.z = 29.0f; 
}

void Player::playerUpdate(Uint8 *keys, GLfloat moveDistance, Camera * cam)
{
	checkBounds();
	if(crouched)
		moveDistance = moveDistance/2;

	if ( keys[SDL_SCANCODE_SPACE] )
	{
		cam->setTempY(3.5f);
		cam->resetIdle();
		currentAnim = 12;
		crouched = true;
	}
	
	if ( keys[SDL_SCANCODE_W] )
	{
		cam->setTempY(3.5f);
		cam->resetIdle();
		pos = rt3d::moveForward(pos,rotate,moveDistance);
		if (crouched)
			currentAnim = 13;
		else
			currentAnim = 1;
	}
	if ( keys[SDL_SCANCODE_S] )
	{
		cam->setTempY(3.5f);
		cam->resetIdle();
		pos = rt3d::moveForward(pos,rotate,-moveDistance);
		if (crouched)
			currentAnim = 13;
		else
			currentAnim = 1;
	}
	if ( keys[SDL_SCANCODE_A] )
	{
		cam->setTempY(3.5f);
		cam->resetIdle();
		pos = rt3d::moveRight(pos,rotate,-moveDistance);
		if (crouched)
			currentAnim = 13;
		else
			currentAnim = 1;
	}
	if ( keys[SDL_SCANCODE_D] )
	{
		cam->setTempY(3.5f);
		cam->resetIdle();
		pos = rt3d::moveRight(pos,rotate,moveDistance);
		if (crouched)
			currentAnim = 13;
		else
			currentAnim = 1;
	}

	if ( keys[SDL_SCANCODE_COMMA] )
	{
		cam->setTempY(3.5f);
		cam->resetIdle();
		rotate -= 2.0f;
		if ( rotate <= -360 )
			rotate = 0.0f;
	}

	if ( keys[SDL_SCANCODE_PERIOD] )
	{
		cam->setTempY(3.5f);
		cam->resetIdle();
		rotate += 2.0f;
		if ( rotate >= 360 )
			rotate= 0.0f;
	}

}