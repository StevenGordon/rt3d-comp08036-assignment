#include "Skybox.h"

Skybox::Skybox()
{
	GLuint skyboxVertCount = 4;

	GLfloat skyboxVerts[12] = {	2.0f, -2.0f, -2.0f,
							2.0f, 2.0f, -2.0f,
							-2.0f, 2.0f, -2.0f,
							-2.0f, -2.0f, -2.0f };

	GLuint skyboxIndexCount = 6;

	GLuint skyboxIndices[6] = { 0,1,2, 0,2,3 };

	GLfloat skyboxTexCoords[8] = {	0.0f, 1.0f,
								0.0f, 0.0f,
								1.0f, 0.0f,
								1.0f, 1.0f };

	int i;

	vertCount = skyboxVertCount;
	
	for(i = 0; i < 12 ; i++)
		verts[i] = skyboxVerts[i];

	indexCount = skyboxIndexCount;

	for(i = 0; i < 6; i++)
		indices[i] = skyboxIndices[i];

	for(i = 0; i < 8; i++)
		texCoords[i] = skyboxTexCoords[i];

	/*
	vertCount = 4;
	verts[12] = {	2.0f, -2.0f, -2.0f,
				2.0f, 2.0f, -2.0f,
				-2.0f, 2.0f, -2.0f,
				-2.0f, -2.0f, -2.0f };

	indexCount = 6;

	indices[6] = ( 0,1,2, 0,2,3 );

	texCoords[8] = (	0.0f, 1.0f,
					0.0f, 0.0f,
					1.0f, 0.0f,
					1.0f, 1.0f );
	*/
}

void Skybox::init()
{
	shader = rt3d::initShaders("textured.vert","textured.frag");

	mesh = rt3d::createMesh(getVertCount(), getVerts(), nullptr,
		getVerts(), getTexCoords(), getIndexCount(), getIndices());

	textures[0] = rt3d::loadBitmap("TropicalDayFront.bmp");
	textures[1] = rt3d::loadBitmap("TropicalDayBack.bmp");
	textures[2] = rt3d::loadBitmap("TropicalDayRight.bmp");
	textures[3] = rt3d::loadBitmap("TropicalDayLeft.bmp");
	textures[4] = rt3d::loadBitmap("TropicalDayUp.bmp");
}

void Skybox::draw(std::stack<glm::mat4> mvStack)
{
		/* Skybox */
	// Front
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(mesh, getIndexCount(), GL_TRIANGLES);

	// Back
	glBindTexture(GL_TEXTURE_2D, textures[3]);
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(mesh,getIndexCount(),GL_TRIANGLES);

	// Left
	glBindTexture(GL_TEXTURE_2D, textures[1]);
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(mesh,getIndexCount(),GL_TRIANGLES);

	// Right
	glBindTexture(GL_TEXTURE_2D, textures[2]);
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(mesh,getIndexCount(),GL_TRIANGLES);

	// Up
	glBindTexture(GL_TEXTURE_2D, textures[4]);
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 0.0f, -1.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(mesh,getIndexCount(),GL_TRIANGLES);

}