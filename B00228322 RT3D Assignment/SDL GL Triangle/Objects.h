#pragma once
#ifndef OBJECTS_H
#define OBJECTS_H

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL.h>
#include <stack>
#include "rt3d.h"
#include "rt3dObjLoader.h"

class Objects
{

public:
	Objects(glm::vec3 iPosition, GLfloat iRotate)
	{ pos.x = iPosition.x; pos.y = iPosition.y; pos.z = iPosition.z; rotate = iRotate; }

	void draw(std::stack<glm::mat4> mvStack, GLuint shader);

	void init(const char *modelFilename, char *textureFileName);

	glm::vec3 getPos() { return pos; }
	void setPos( glm::vec3 set )
	{
		pos.x = set.x;
		pos.y = set.y;
		pos.z = set.z;
	}

	void addRotate(GLfloat angle) { rotate += angle; }

	GLuint getTexture() { return texture; }

	GLuint getMesh() { return mesh; }

	GLuint getVertCount() { return meshIndexCount; }

	std::vector<GLfloat> getVerts() { return verts; }
	std::vector<GLfloat> getNorms() { return norms; }
	std::vector<GLfloat> getTex_coords() { return tex_coords; }
	std::vector<GLuint> getIndices() { return indices; }

	virtual ~Objects() { return; }

protected:

	glm::vec3 pos;
	glm::vec2 colBox;

	GLfloat rotate;

	GLuint texture;
	GLuint mesh;
	GLuint meshIndexCount;

	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;

	GLuint size;

};

#endif