#pragma once
#ifndef ENEMY_H
#define ENEMY_H

#define RADIAN_TO_DEG 57.29577951

#include "rt3d.h"
#include "Npc.h"
#include "Player.h"
#include <math.h>
#include <SDL.h>
#include <glm/glm.hpp>

class Enemy : public Npc
{

public:

	Enemy(glm::vec3 iPosition, GLfloat iRotate);

	void draw(std::stack<glm::mat4> mvStack, GLuint shader, GLfloat scale);

	void alert(GLfloat moveDistance, Player * player);

	void enemyUpdate(GLfloat moveDistance, Player * player);

	~Enemy() { }

private:
	bool canSeePlayer;
	bool hostile;

	GLfloat playerDistanceX;
	GLfloat playerDistanceZ;
};

#endif