#include "Camera.h"

Camera::Camera(glm::vec3 position, glm::vec3 target, glm::vec3 orientation)
{
	eye = position;
	at = target;
	up = orientation;
	idleR = 0.0f;

	timer = 0;
	idleTimer = 5000;
}

void Camera::CameraUpdate(Uint8 *keys, GLfloat r)
{
	tempEye.y = eye.y;

	if ( keys[SDL_SCANCODE_R] )
	{
		if (tempEye.y <= 10.5)
			tempEye.y += 1.0f;
	}
	if ( keys[SDL_SCANCODE_F] )
	{
		if (tempEye.y >= 1.5)
			tempEye.y -= 1.0f;
	}
	
	idleRotate();
}

void Camera::idleRotate()
{
	if ( idle )
	{
		idleR += 0.5f;
		if ( idleR >= 360 )
			idleR = 0;
	}
	else
	{
		idleR = 90.0f;
	}
}

void Camera::resetIdle()
{
	timer = 0;
	idle = false;
}

Camera::~Camera() { }